function invalidNumber([args1]){
    let num = Number(args1);
    let inRange = (num >= 100 && num <= 200) || num == 0;

    if(!inRange){
        console.log("invalid");
    }
}

invalidNumber([1]);