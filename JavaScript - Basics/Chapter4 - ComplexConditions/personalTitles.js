function personalTitles([args1, args2]){
    let age = parseInt(args1);
    let gender = args2;

    if(age >= 16){
        if(gender == "m"){
            console.log("Master");
        }else{
            console.log("Miss");
        }
    }else{
        if(gender == "m"){
            console.log("Mr.");
        }else{
            console.log("Mrs.");
        }
    }
}

personalTitles([4,"m"]);