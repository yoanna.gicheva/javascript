function typeOfAnimal([args1]){
    let animal = args1;

    switch(animal){
        case "dog": console.log("mammal"); break;
        case "crocodile":
        case "tortoise":
        case "snake":
            console.log("reptile"); break;
        default:
            console.log("unknown"); break;
    }
}

typeOfAnimal(["tortoise"]);