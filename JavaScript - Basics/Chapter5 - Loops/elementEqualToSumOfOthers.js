function elementEqualToSum(args){
    let n = args[0];
    let currentNum = 0;
    let currentSum = 0;


    for(let i = 1; i < n; i++){
        currentNum = args[i];
        for(let k = 1; k < n; k++){
            if(k != i){
                currentSum += args[k];
            }
        }
        if(currentNum == currentSum){
            console.log("Yes");
            console.log("Sum = " + currentSum);
            break;
        }
        currentSum = 0;
    }
}