function algorithm(a,b){
    var oldB = 0;
    while(b != 0){
        oldB = b;
        b = a % b;
        a = oldB;
    }
    console.log(a);
}

algorithm(6, 8);