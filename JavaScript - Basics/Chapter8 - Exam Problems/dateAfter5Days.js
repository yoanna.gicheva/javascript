function dateAfter5Days(d, m){
    let monthDays = 0;
    if(m == 2){
        monthDays = 28;
    }else if(m == 4 || m == 6 || m == 9 || m == 11){
        monthDays = 30;
    }else{
        monthDays = 31;
    }

    let nextDays = d+5;
    let nextMonth = m;
    if(nextDays > monthDays){
        nextDays = nextDays - monthDays;
        nextMonth++;
    }
    if(nextMonth < 10){
        console.log(nextDays+".0"+nextMonth);
    }else{
        console.log(nextDays+"."+nextMonth);
    }
}

dateAfter5Days(28,03);