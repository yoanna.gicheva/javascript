function increasingElements(numbers){
    let n = Number(numbers[0]);
    let maxElements = 1;
    let currentMax = 1;
    for(let i = 1; i < n - 1; i++){
        if(numbers[i] < numbers[i+1]){
            currentMax++;
            if(currentMax > maxElements){
                maxElements = currentMax;
            }
        }else{
            currentMax = 1;
        }
    }
    console.log(maxElements);
}

increasingElements([3,2,5,4]);
