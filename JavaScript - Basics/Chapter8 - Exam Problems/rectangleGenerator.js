function rectangleGenerator(n, m){
    let count = 0;
    for(let left = -n; left < n; left++){
        for(let top = -n; top < n; top ++){
            for(let right = left + 1; right <= n; right++){
                for(let bottom = top + 1; bottom <= n; bottom ++){
                    let area = Math.abs(left - right) * Math.abs(top - bottom);

                    if(area >= m){
                        count++;
                        console.log(`(${left},${right}) (${top},${bottom}) -> ${area}`);
                    }
                }
            }
        }
        
    }

    if(count == 0){
        console.log("No");
    }
}

rectangleGenerator(1,2);