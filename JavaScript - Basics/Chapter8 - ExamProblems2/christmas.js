function tree([n]){
    let rows = [];
    let width = 4*n + 1;
    let height = 2*n + 5;

    let dotsAtEnds = (width - 3)/2;
    rows.push('.'.repeat(dotsAtEnds) + "/|\\" + '.'.repeat(dotsAtEnds));
    rows.push('.'.repeat(dotsAtEnds) + "\\|/" + '.'.repeat(dotsAtEnds));
    rows.push('.'.repeat(dotsAtEnds) + "***" + '.'.repeat(dotsAtEnds));

    let betweenStars = 1;
    height -= 6;
    dotsAtEnds -= 2;

    for(let i = 1; i <= height; i++){
       rows.push(".".repeat(dotsAtEnds) + "*"+"-".repeat(betweenStars) +"*" + "-".repeat(betweenStars) + "*" + ".".repeat(dotsAtEnds));
       betweenStars++;
       dotsAtEnds -= 2;
    }

    rows.push('*'.repeat(width));
    let row = "*";
    for(let i = 1; i < width; i++){
        if(i % 2 == 0){
            row += "*";
        }else{
            row += ".";
        }
    }
    rows.push(row);
    rows.push('*'.repeat(width));

    for(let i = 0; i < rows.length; i++){
        console.log(rows[i]);
    }
}

tree([4]);