function flowerShop([numHrizantem, numRoses, numTulips, season, holiday]){
   let priceHrizantem = 0;
   let priceRoses = 0;
   let priceTulips = 0;

   if(season == "Spring" || season == "Winter"){
       priceHrizantem = 2.0;
       priceRoses = 4.10;
       priceTulips = 2.50;
   }else if(season == "Summer" || season == "Autumn"){
       priceHrizantem = 3.75;
       priceRoses = 4.50;
       priceTulips = 4.15;
   }

   let totalHrizantem = priceHrizantem*numHrizantem;
   let totalRoses = priceRoses * numRoses;
   let totalTulips = priceTulips * numTulips;
   let bucket = totalHrizantem + totalRoses + totalTulips;

   if(holiday == "Y"){
           bucket += bucket*15/100;
       if(numTulips > 7 && season == "Spring"){
            bucket -= bucket*5/100;
       }
       if(numRoses >= 10 && season == "Winter"){
            bucket -= bucket*10/100;
       }
       if(numHrizantem + numRoses + numTulips > 20){
            bucket -= bucket*20/100;
       }
   }

   bucket += 2;
   console.log(bucket.toFixed(2));
}

flowerShop([2,4,8, "Spring", "Y"])