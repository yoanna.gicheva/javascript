function grades(grades){
    let n = grades[0];
    let top = 0;
    let middle = 0;
    let poor = 0;
    let fail = 0;
    let avgSum = 0;
    let grade = 0;


    for(let i = 1; i <= n; i++){
        grade = grades[i];
        if(grade >= 2 && grade <= 2.99){
            fail++;
        }else if(grade >= 3 && grade <= 3.99){
            poor++;
        }else if(grade >= 4 && grade <= 4.99){
            middle++;
        }else if(grade >= 5){
            top++;
        }
        avgSum += grade;
    }

    let topPercent = top*100/n;
    let middlePercent = middle*100/n;
    let poorPercent = poor*100/n;
    let failPercent = fail*100/n;
    let avg = avgSum/n;

    console.log(`Top students: ${topPercent.toFixed(2)}%`);
    console.log(`Between 4.00 and 4.99: ${middlePercent.toFixed(2)}%`);
    console.log(`Between 3.00 and 3.99: ${poorPercent.toFixed(2)}%`);
    console.log(`Fail: ${failPercent.toFixed(2)}%`);
    console.log(`Average: ${avg.toFixed(2)}`);
}

grades([10, 3.00, 2.99, 5.68, 3.01, 4, 4, 6, 4.50, 2.44, 5]);